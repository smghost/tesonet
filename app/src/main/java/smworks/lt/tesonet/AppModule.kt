package smworks.lt.tesonet

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import smworks.lt.tesonet.data.RestClient
import smworks.lt.tesonet.data.repositories.PreferenceRepositoryImpl
import smworks.lt.tesonet.data.repositories.ServerRepositoryImpl
import smworks.lt.tesonet.data.repositories.TokenRepositoryImpl
import smworks.lt.tesonet.domain.repositories.PreferenceRepository
import smworks.lt.tesonet.domain.repositories.ServerRepository
import smworks.lt.tesonet.domain.repositories.TokenRepository
import smworks.lt.tesonet.domain.usecases.LogoutUC
import smworks.lt.tesonet.domain.usecases.GetServerListUC
import smworks.lt.tesonet.domain.usecases.LoginUC
import smworks.lt.tesonet.presentation.login.LoginViewModel
import smworks.lt.tesonet.presentation.main.MainViewModel

val appModule = module {
    single<RestClient> {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://playground.tesonet.lt/v1/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        retrofit.create(RestClient::class.java)
    }
    single<TokenRepository> { TokenRepositoryImpl(get())}
    single<ServerRepository> { ServerRepositoryImpl(get())}
    single<PreferenceRepository> { PreferenceRepositoryImpl(get()) }

    factory { LoginUC(get(), get()) }
    factory { GetServerListUC(get(), get()) }
    factory { LogoutUC(get()) }

    viewModel { MainViewModel(get(), get(), get()) }
    viewModel { LoginViewModel(get()) }
}