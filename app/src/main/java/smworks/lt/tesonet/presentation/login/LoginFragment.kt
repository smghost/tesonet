package smworks.lt.tesonet.presentation.login

import androidx.navigation.Navigation
import org.koin.android.viewmodel.ext.android.viewModel
import smworks.lt.tesonet.R
import smworks.lt.tesonet.databinding.LoginFragmentBinding
import smworks.lt.tesonet.presentation.BaseFragment

class LoginFragment(override val layoutId: Int = R.layout.login_fragment) : BaseFragment<LoginFragmentBinding>() {

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(binding: LoginFragmentBinding) {
        binding.loginViewModel = loginViewModel
        setupObservables()
        setupNavigation(binding)
    }

    private fun setupObservables() {
        observeProgressAndError(loginViewModel)
    }

    private fun setupNavigation(binding: LoginFragmentBinding) {
        loginViewModel.exit.observe(viewLifecycleOwner) {
            Navigation.findNavController(binding.root).popBackStack()
        }
    }
}