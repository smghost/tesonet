package smworks.lt.tesonet.presentation.main

import android.widget.LinearLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import org.koin.android.viewmodel.ext.android.viewModel
import smworks.lt.tesonet.R
import smworks.lt.tesonet.databinding.MainFragmentBinding
import smworks.lt.tesonet.presentation.BaseFragment

class MainFragment(override val layoutId: Int = R.layout.main_fragment) : BaseFragment<MainFragmentBinding>() {

    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreateView(binding: MainFragmentBinding) {
        binding.mainViewModel = mainViewModel
        val serverAdapter = ServerAdapter()
        binding.list.adapter = serverAdapter
        val dividerItemDecoration = DividerItemDecoration(requireContext(), LinearLayout.VERTICAL)
        binding.list.addItemDecoration(dividerItemDecoration)
        setupObservables(serverAdapter)
        setupNavigation(binding)
        mainViewModel.loadServerList()
    }

    private fun setupNavigation(binding: MainFragmentBinding) {
        mainViewModel.navigateToLogin.observe(viewLifecycleOwner) {
            Navigation.findNavController(binding.root)
                    .navigate(R.id.action_mainFragment_to_loginFragment)
        }
    }

    private fun setupObservables(serverAdapter: ServerAdapter) {
        mainViewModel.serverList.observe(viewLifecycleOwner) {
            serverAdapter.submitList(it)
        }
        observeProgressAndError(mainViewModel)
    }
}