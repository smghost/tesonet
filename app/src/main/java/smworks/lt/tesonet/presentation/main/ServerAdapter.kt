package smworks.lt.tesonet.presentation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import smworks.lt.tesonet.databinding.MainFragmentListItemBinding
import smworks.lt.tesonet.domain.models.Server

class ServerAdapter : ListAdapter<Server, ServerAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Server>() {
            override fun areItemsTheSame(oldItem: Server, newItem: Server) = oldItem === newItem
            override fun areContentsTheSame(oldItem: Server, newItem: Server) = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(MainFragmentListItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
    inner class ViewHolder(private val binding: MainFragmentListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(server: Server) {
            binding.model = server
            binding.executePendingBindings()
        }
    }
}