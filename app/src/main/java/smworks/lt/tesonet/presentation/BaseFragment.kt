package smworks.lt.tesonet.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import smworks.lt.tesonet.R
import timber.log.Timber

abstract class BaseFragment<T : ViewDataBinding> : Fragment() {

    abstract val layoutId: Int

    abstract fun onCreateView(binding: T)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        Timber.d("onCreateView()")
        val binding = DataBindingUtil.inflate(
                inflater, layoutId, container, false
        ) as T
        binding.lifecycleOwner = viewLifecycleOwner
        onCreateView(binding)
        return binding.root
    }

    fun observeProgressAndError(baseViewModel: BaseViewModel) {
        baseViewModel.error.observe(viewLifecycleOwner) {
            view?.let { view ->
                Snackbar.make(view, it, Snackbar.LENGTH_LONG).show()
            }
        }
    }
}