package smworks.lt.tesonet.presentation.login

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.usecases.LoginUC
import smworks.lt.tesonet.presentation.BaseViewModel
import timber.log.Timber

class LoginViewModel(private val loginUC: LoginUC) : BaseViewModel() {

    fun clickSubmit(username: String, password: String) = viewModelScope.launch {
        _isInProgress.value = true
        when (val output = loginUC(username, password)) {
            is Output.Success -> _exit.value = Unit
            is Output.Failure -> {
                Timber.e(output.throwable)
                _error.value = output.throwable.message
            }
        }
        _isInProgress.value = false
    }

}