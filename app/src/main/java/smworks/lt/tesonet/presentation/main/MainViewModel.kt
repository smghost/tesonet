package smworks.lt.tesonet.presentation.main

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.launch
import smworks.lt.tesonet.R
import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Server
import smworks.lt.tesonet.domain.usecases.LogoutUC
import smworks.lt.tesonet.domain.usecases.GetServerListUC
import smworks.lt.tesonet.presentation.BaseViewModel
import timber.log.Timber

class MainViewModel(
        private val context: Context,
        private val getServerListUC: GetServerListUC,
        private val logoutUC: LogoutUC
) : BaseViewModel() {

    private val _serverList = MutableLiveData<List<Server>>(listOf())
    val serverList: LiveData<List<Server>> = _serverList

    private val _navigateToLogin = LiveEvent<Unit>()
    val navigateToLogin: LiveData<Unit> = _navigateToLogin

    fun loadServerList() = viewModelScope.launch(exceptionHandler) {
        _isInProgress.value = true
        when (val output = getServerListUC()) {
            is Output.Success -> _serverList.value = output.result
            is Output.Failure -> {
                if (output.throwable is GetServerListUC.NoTokenFoundException) {
                    _navigateToLogin.value = Unit
                } else {
                    Timber.e(output.throwable)
                    _error.value = context.getString(R.string.unable_to_download_server_list)
                }
            }
        }
        _isInProgress.value = false
    }

    fun clickLogout() {
        logoutUC()
        _navigateToLogin.value = Unit
    }
}