package smworks.lt.tesonet.data.repositories

import smworks.lt.tesonet.data.RestClient
import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Token
import smworks.lt.tesonet.domain.models.TokenRequest
import smworks.lt.tesonet.domain.repositories.TokenRepository

class TokenRepositoryImpl(private val restClient: RestClient) : TokenRepository {

    override suspend fun getToken(username: String, password: String): Output<Token> {
        return try {
            Output.Success(restClient.requestToken(TokenRequest(username, password)))
        } catch (e: Throwable) {
            Output.Failure(e)
        }
    }

}