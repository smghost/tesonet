package smworks.lt.tesonet.data

import retrofit2.http.*
import smworks.lt.tesonet.domain.models.Server
import smworks.lt.tesonet.domain.models.Token
import smworks.lt.tesonet.domain.models.TokenRequest

interface RestClient {
    @Headers("Content-Type: application/json")
    @POST("tokens")
    suspend fun requestToken(@Body tokenRequest: TokenRequest): Token

    @GET("servers")
    suspend fun getServers(@Header("Authorization") token: String): List<Server>
}