package smworks.lt.tesonet.data

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.io.IOException
import java.lang.reflect.Type
import java.util.*

object MoshiUtils {
    @JvmStatic
    fun <T> toString(objectClass: Class<T>, serverList: List<T>): String {
        val moshi = Moshi.Builder().build()
        val type: Type = Types.newParameterizedType(MutableList::class.java, objectClass)
        val jsonAdapter: JsonAdapter<List<T>> = moshi.adapter(type)
        return jsonAdapter.toJson(serverList)
    }

    @JvmStatic
    fun <T> fromString(objectClass: Class<T>, list: String): List<T> {
        val moshi = Moshi.Builder().build()
        val type: Type = Types.newParameterizedType(MutableList::class.java, objectClass)
        val jsonAdapter: JsonAdapter<List<T>> = moshi.adapter(type)
        return try {
            jsonAdapter.fromJson(list) ?: listOf()
        } catch (e: IOException) {
            ArrayList()
        }
    }
}