package smworks.lt.tesonet.data.repositories

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import smworks.lt.tesonet.data.MoshiUtils
import smworks.lt.tesonet.domain.models.Server
import smworks.lt.tesonet.domain.repositories.PreferenceRepository

class PreferenceRepositoryImpl(appContext: Context) : PreferenceRepository {

    private val sharedPreferences = appContext.getSharedPreferences("preferences", AppCompatActivity.MODE_PRIVATE)

    companion object {
        const val TOKEN = "token"
        const val SERVER_LIST = "server_list"
    }

    private fun getPreference(key: String): String {
        return sharedPreferences.getString(key, "") ?: ""
    }

    private fun setPreference(key: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override var token: String
        get() = getPreference(TOKEN)
        set(value) {
            setPreference(TOKEN, value)
        }

    override var serverList: List<Server>
        get() = MoshiUtils.fromString(Server::class.java, getPreference(SERVER_LIST))
        set(value) {
            setPreference(SERVER_LIST, MoshiUtils.toString(Server::class.java, value))
        }
}