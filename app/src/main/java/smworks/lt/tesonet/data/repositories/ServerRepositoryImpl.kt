package smworks.lt.tesonet.data.repositories

import smworks.lt.tesonet.data.RestClient
import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Server
import smworks.lt.tesonet.domain.models.Token
import smworks.lt.tesonet.domain.models.TokenRequest
import smworks.lt.tesonet.domain.repositories.ServerRepository
import smworks.lt.tesonet.domain.repositories.TokenRepository

class ServerRepositoryImpl(private val restClient: RestClient) : ServerRepository {
    
    override suspend fun getServers(token: String): Output<List<Server>> {
        return try {
            Output.Success(restClient.getServers(token))
        } catch (e: Throwable) {
            Output.Failure(e)
        }
    }

}