package smworks.lt.tesonet.domain.usecases

import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Server
import smworks.lt.tesonet.domain.repositories.PreferenceRepository
import smworks.lt.tesonet.domain.repositories.ServerRepository
import java.lang.Exception

class GetServerListUC(private val serverRepository: ServerRepository,
                      private val preferenceRepository: PreferenceRepository) {

    suspend operator fun invoke(): Output<List<Server>> {
        if (preferenceRepository.token.isBlank()) {
            return Output.Failure(NoTokenFoundException)
        }
        val cachedServerList = preferenceRepository.serverList
        if (cachedServerList.isNotEmpty()) {
            return Output.Success(cachedServerList)
        }
        val serverOutput = serverRepository.getServers(preferenceRepository.token)

        if (serverOutput is Output.Success) {
            preferenceRepository.serverList = serverOutput.result
        }

        return serverOutput
    }

    object NoTokenFoundException : Exception("No token found")
}