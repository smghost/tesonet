package smworks.lt.tesonet.domain.models

data class Token(val token: String)