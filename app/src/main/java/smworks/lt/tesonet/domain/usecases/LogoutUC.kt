package smworks.lt.tesonet.domain.usecases

import smworks.lt.tesonet.domain.repositories.PreferenceRepository

class LogoutUC(private val preferenceRepository: PreferenceRepository) {

    operator fun invoke() {
        preferenceRepository.serverList = listOf()
        preferenceRepository.token = ""
    }
}