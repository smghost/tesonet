package smworks.lt.tesonet.domain.repositories

import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Token

interface TokenRepository {
    suspend fun getToken(username: String, password: String): Output<Token>
}