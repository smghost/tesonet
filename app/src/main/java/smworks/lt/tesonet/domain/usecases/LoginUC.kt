package smworks.lt.tesonet.domain.usecases

import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.repositories.PreferenceRepository
import smworks.lt.tesonet.domain.repositories.TokenRepository

class LoginUC(private val tokenRepository: TokenRepository,
              private val preferenceRepository: PreferenceRepository) {

    suspend operator fun invoke(username: String, password: String): Output<Boolean> {
        return when (val output = tokenRepository.getToken(username, password)) {
            is Output.Success -> {
                preferenceRepository.token = output.result.token
                Output.Success(true)
            }
            is Output.Failure -> output
        }
    }
}