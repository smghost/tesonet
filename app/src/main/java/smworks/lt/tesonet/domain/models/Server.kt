package smworks.lt.tesonet.domain.models

data class Server(val name: String, val distance: String)