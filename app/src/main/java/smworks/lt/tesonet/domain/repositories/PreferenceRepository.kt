package smworks.lt.tesonet.domain.repositories

import smworks.lt.tesonet.domain.models.Server

interface PreferenceRepository {
    var token: String
    var serverList: List<Server>
}