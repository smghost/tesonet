package smworks.lt.tesonet.domain.repositories

import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Server

interface ServerRepository {
    suspend fun getServers(token: String): Output<List<Server>>
}