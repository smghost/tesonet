package smworks.lt.tesonet.domain.models

data class TokenRequest(val username: String, val password: String)