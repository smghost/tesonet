package smworks.lt.tesonet.domain.common

sealed class Output<out T : Any> {
    data class Success<out T : Any>(val result: T) : Output<T>()
    data class Failure(val throwable: Throwable) : Output<Nothing>()
}