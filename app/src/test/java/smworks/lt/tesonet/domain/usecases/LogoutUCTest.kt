package smworks.lt.tesonet.domain.usecases

import io.kotest.core.spec.style.BehaviorSpec
import io.mockk.mockk
import io.mockk.verify
import smworks.lt.tesonet.domain.repositories.PreferenceRepository

class LogoutUCTest : BehaviorSpec({
    Given("LogoutUCTest") {
        val preferenceRepository = mockk<PreferenceRepository>(relaxed = true)
        val logoutUC = LogoutUC(preferenceRepository)
        When("logout is successful") {
            Then("token is stored in preferences") {
                logoutUC()
                verify(exactly = 1) { preferenceRepository.token = any() }
                verify(exactly = 1) { preferenceRepository.serverList = any() }
            }
        }
    }
})