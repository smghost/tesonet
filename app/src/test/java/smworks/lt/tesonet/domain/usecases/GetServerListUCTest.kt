package smworks.lt.tesonet.domain.usecases

import io.kotest.core.spec.style.BehaviorSpec
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Server
import smworks.lt.tesonet.domain.repositories.PreferenceRepository
import smworks.lt.tesonet.domain.repositories.ServerRepository

class GetServerListUCTest : BehaviorSpec({
    Given("GetServerListUC") {
        val serverRepository = mockk<ServerRepository>()
        val preferenceRepository = mockk<PreferenceRepository>(relaxed = true)
        val getServerListUC = GetServerListUC(serverRepository, preferenceRepository)

        When("no token is stored") {
            every { preferenceRepository.token } returns ""
            Then("NoTokenFoundException is thrown") {
                val output = getServerListUC()
                assert(output is Output.Failure)
            }
        }

        When("request is successful") {
            val serverList = listOf(Server("name", "1010"))
            coEvery { serverRepository.getServers(any()) } returns Output.Success(serverList)
            every { preferenceRepository.token } returns "some token"
            Then("server list is returned") {
                val output = getServerListUC()
                assert(output is Output.Success)
                verify(exactly = 1) { preferenceRepository.serverList = any() }
            }
        }

        When("servers are already cached") {
            val serverList = listOf(Server("name", "1010"))
            every { preferenceRepository.token } returns "some token"
            every { preferenceRepository.serverList } returns serverList
            Then("server list is returned from cache") {
                val output = getServerListUC()
                assert(output is Output.Success)
            }
        }
    }
})