package smworks.lt.tesonet.domain.usecases

import io.kotest.core.spec.style.BehaviorSpec
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import smworks.lt.tesonet.domain.common.Output
import smworks.lt.tesonet.domain.models.Token
import smworks.lt.tesonet.domain.repositories.PreferenceRepository
import smworks.lt.tesonet.domain.repositories.TokenRepository

class LoginUCTest : BehaviorSpec({
    Given("LoginUCTest") {
        val tokenRepository = mockk<TokenRepository>()
        val preferenceRepository = mockk<PreferenceRepository>(relaxed = true)
        val loginUC = LoginUC(tokenRepository, preferenceRepository)
        When("login is successful") {
            coEvery { tokenRepository.getToken(any(), any()) } returns Output.Success(Token("token"))
            Then("token is stored in preferences") {
                loginUC("username", "password")
                verify(exactly = 1) { preferenceRepository.token = any() }
            }
        }
    }
})